import $ from 'jquery';

import 'bootstrap/js/dist/dropdown';

import './module/header';
import './module/map';

window.$ = $;
window.jQuery = $;