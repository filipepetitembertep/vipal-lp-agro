import $ from 'jquery';

document.addEventListener('DOMContentLoaded', function () {
    $.getScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyCim7b6dP4k8sGo7ENqntqvAfdCQ56nu7Q', () => {

        const places = [
            {
                country: 'Brasil',
                state: 'RS',
                city: 'Porto Alegre',
                type: 'rede autorizada',
                geometry: {
                    type: 'Point',
                    coordinates: [
                        -29.988367,
                        -51.1724207
                    ]
                }
            },
            {
                country: 'Brasil',
                state: 'RS',
                city: 'Novo Hamburgo',
                type: 'rede autorizada',
                geometry: {
                    type: 'Point',
                    coordinates: [
                        -29.6644737,
                        -51.1598495
                    ]

                }
            },
            {
                country: 'Brasil',
                state: 'RS',
                city: 'Novo Hamburgo',
                type: 'distribuidor nas três opções',
                geometry: {
                    type: 'Point',
                    coordinates: [
                        -30.6644737,
                        -52.1598495
                    ]

                }
            },
            {
                country: 'Argentina',
                state: 'SF',
                city: 'Rosário',
                type: 'distribuidor nas três opções',
                geometry: {
                    type: 'Point',
                    coordinates: [
                        -32.9544132,
                        -60.6624401
                    ]

                }
            }

        ];

        let locations = places;

        const map = new google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            center: { lat: -29.988367, lng: -51.1724207 }
        });

        let markers = locations.map(function (location) {
            return new google.maps.Marker({
                position: { lat: location.geometry.coordinates[0], lng: location.geometry.coordinates[1] },
                icon: '/img/frontend/general/pin.svg'
            });
        });

        /* let markerCluster = new MarkerClusterer(map, markers, {
            imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
        }); */

        let locationFiltered = [];

        const values = ['Brasil', 'RS', 'Porto Alegre'];
        let valRadio = $('.radio-button[name="type"]').val();
        valRadio = 'todos';

        $('.filter-map .radio-button').not('.all-filter').on('change', function () {
            locations = places;
            // console.log(locations)
            valRadio = $(this).val();
            locationFiltered = [];

            locations.forEach(function (item, i) {
                values.forEach(function (value, obj) {
                    if (item.city.indexOf(value) > -1 && item.type === valRadio) {
                        locationFiltered.push(item);
                        locations = locationFiltered;
                        reloadMap();

                    } 
                    if (item.country.indexOf(value) > -1 && item.type === valRadio) {
                        locationFiltered.push(item);
                        locations = locationFiltered;
                        reloadMap();

                    }
                    if (item.state.indexOf(value) > -1 && item.type === valRadio) {
                        locationFiltered.push(item);
                        locations = locationFiltered;
                        reloadMap();

                    }
                });

            });
        });

        $('.filter-map .all-filter').on('change', function () {
            locations = places;
            locationFiltered = [];
            //console.log(locations)

            locations.forEach(function (item, i) {
                values.forEach(function (value, obj) {
                    if (item.city.indexOf(value) > -1) {
                        locationFiltered.push(item);
                        locations = locationFiltered;
                        reloadMap();
                    }
                    if (item.country.indexOf(value) > -1) {
                        locationFiltered.push(item);
                        locations = locationFiltered;
                        reloadMap();
                    }
                    if (item.state.indexOf(value) > -1) {
                        locationFiltered.push(item);
                        locations = locationFiltered;
                        reloadMap();
                    }
                });

            });
        });

        function reloadMap() {

            map.panTo({ lat: locations[0].geometry.coordinates[0], lng: locations[0].geometry.coordinates[1] })

            //console.log(locations)
            for (let i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }
            markers = [];
            locations.forEach(function (item, i) {
                let marker = new google.maps.Marker({
                    position: { lat: item.geometry.coordinates[0], lng: item.geometry.coordinates[1] },
                    icon: '/img/frontend/general/pin.svg',
                    map: map
                });
                markers.push(marker);
            });
        }
    });
});
