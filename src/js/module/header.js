import $ from 'jquery';

$('.main-header .mobile-menu .btn-hamburger').click(function () {
    if ($(this).hasClass('active')) {
        // $('.menu-items .menu-item .submenu-items.collapse.show').removeClass('show');
        $('.menu-items ul.active').removeClass('active');
    }

    $(this).toggleClass('active');
    $('.circle').toggleClass('expand');
    $('.menu-items').toggleClass('active');
    $('body').toggleClass('disable-scroll');

});